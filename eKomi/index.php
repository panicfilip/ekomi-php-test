<?php
require_once ('core/MySQLiDB.php');
require_once ('core/TwitterWrapper.php');
require_once ('core/CacheController.php');

//get tweets from cache
$cacheObject = (object) array('type' => 'memcache', 'key' => 'eKomiTweets');
$memcacheStrategy = new CacheController($cacheObject);
$tweets = $memcacheStrategy->get();
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>eKomi Tweets PHP test</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="eKomi Tweets PHP test">
    <meta name="author" content="Filip Panić">    
    <link rel="shortcut icon" href="favicon.ico">  
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Covered+By+Your+Grace' rel='stylesheet' type='text/css'>  
    <!-- Global CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="css/styles.css">
</head> 

<body data-spy="scroll">
    <!-- ******HEADER****** --> 
    <header id="top" class="header navbar-fixed-top">  
        <div class="container">            
            <h1 class="logo pull-left">
                <a class="scrollto" href="#promo">
                    <img id="logo-image" class="logo-image" src="img/logo.png" alt="Logo">
                </a>
            </h1><!--//logo-->              
            <nav id="main-nav" class="main-nav navbar-right" role="navigation">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button><!--//nav-toggle-->
                </div><!--//navbar-header-->            
                <div class="navbar-collapse collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="nav-item"><a href="http://www.ekomi-us.com/us/#why-ekomi">Why eKomi</a></li>
                        <li class="nav-item"><a href="http://www.ekomi-us.com/us/#how-ekomi-works">How eKomi works</a></li>
                        <li class="nav-item"><a href="http://www.ekomi-us.com/us/#listen-clients">References</a></li>
                        <li class="nav-item last"><a href="http://www.ekomi-us.com/us/about-ekomi/">About</a></li>
                    </ul><!--//nav-->
                </div><!--//navabr-collapse-->
            </nav><!--//main-nav-->           
        </div>
    </header><!--//header-->
    
    <!-- ******PROMO****** --> 
    <section id="promo" class="promo section offset-header has-pattern">
        <div class="container">
            <div class="row">
                <div class="overview col-md-8 col-sm-12 col-xs-12">
                    
                    
                </div><!--//overview-->
                
                <!--// iPhone starts -->
                <div class="phone iphone iphone-black col-md-4 col-sm-12 col-xs-12 ">
                    <div class="iphone-holder phone-holder">
                        <div class="iphone-holder-inner">
                            <div class="slider flexslider">
                                
                            </div><!--//flexslider-->   
                        </div><!--//iphone-holder-inner-->                    
                    </div><!--//iphone-holder-->                   
                </div><!--//iphone-->  
                <!--// iPhone ends -->                                           
            </div><!--//row-->
        </div><!--//container-->
    </section><!--//promo-->

    <!-- ******TESTIMONIALS****** --> 
    <section id="testimonials" class="testimonials section">
        <div class="container">
            <div class="row">
                <h2 class="title text-center">Ekomi tweets</h2>
                <?php if(sizeof($tweets)>0){ ?>
                    <?php for ($i=0; $i <sizeof($tweets) ; $i++) { ?>
                   
                        <div class="item col-lg-8 col-md-8 col-sm-8 col-xs-10 <?php if($i%2==0){ echo 'pull-right'; } ?>">
                            <div class="quote-box">
                                <i class="fa fa-quote-left"></i>
                                <blockquote class="quote">
                                    <?php if($tweets[$i]['retweeted'] == 1){ ?>eKomi retweeted<br><?php } ?>
                                    <a href="https://twitter.com/<?php echo $tweets[$i]['tweet_user_screen_name']; ?>" target="blank">@<?php echo $tweets[$i]['tweet_user_screen_name']; ?></a> <?php echo $tweets[$i]['tweet_text']; ?><br>
                                    <form method="post" action="reply.php">
                                        <input type="text" name="status" id="status" class="status_input" placeholder="Reply">
                                        <input type="hidden" name="tweet_id" id="tweet_id" value="<?php echo $tweets[$i]['tweet_id']; ?>">
                                        <button type="submit" title="Reply" type="button" class="btn btn-default" aria-label="Left Align">
                                          <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                                        </button>
                                    </form>
                                    <button onclick="location.href='retweet/<?php echo $tweets[$i]['tweet_id']; ?>'" title="Retweet" type="button" class="btn btn-default" aria-label="Left Align">
                                      <span class="glyphicon glyphicon-retweet" aria-hidden="true"> <?php echo $tweets[$i]['retweet_count']; ?></span>
                                    </button>
                                    <button onclick="location.href='favorite/<?php echo $tweets[$i]['tweet_id']; ?>'" title="Favorite" type="button" class="btn btn-default" aria-label="Left Align">
                                      <span class="glyphicon glyphicon-star" aria-hidden="true"> <?php echo $tweets[$i]['favorite_count']; ?></span>
                                    </button>
                                </blockquote><!--//quote-->
                            </div><!--//quote-box-->
                            <div class="people row">
                                <img class="img-rounded user-pic col-md-5 col-sm-5 col-xs-12 col-md-offset-1 col-sm-offset-1" src="<?php echo $tweets[$i]['tweet_user_profile_image_url']; ?>" alt="" />
                                <p class="details text-center pull-left">
                                    <span class="name"><?php echo $tweets[$i]['tweet_user_name']; ?></span>
                                    <span class="title"><?php echo $tweets[$i]['tweet_date_created']; ?></span>
                                </p>                        
                            </div><!--//people-->
                        </div><!--//item-->

                    <?php } ?>
                <?php } ?>    
                
            </div><!--//row-->
            
        </div><!--//container-->
    </section><!--//Testimonials-->
      
    <!-- ******FOOTER****** --> 
    <footer class="footer">
        <div class="container">
            <small class="copyright pull-left">Copyright &copy; 2015 Filip Panić</a></small>
            <ul class="links list-inline">
                <li><a href="http://www.ekomi-us.com/us/terms-and-conditions/">Terms</a></li>
                <li><a href="http://www.ekomi-us.com/us/privacy/">Privacy</a></li>
            </ul>
        </div>
    </footer><!--//footer-->   

    <!-- Javascript --> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>
</html>