<?php
require_once('StrategyInterface.php');

class MemcacheStrategy implements StrategyInterface
{

	private $memcache;

	function __construct()
	{
		$memcache = new Memcache;
		$this->setMemcache($memcache);
		$memcache->addServer('localhost', 11211);
		$stats = @$memcache->getExtendedStats();
		$available = (bool) $stats["localhost:11211"];
		if ($available && @$memcache->connect('localhost', 11211)){
		    // MemCache is there
		}
		else{
	    	die('Not connected');
		}
	}

	public function set($key, $tmp_object)
	{
		$compression = false;
		$expire_time = 6000;

		return $this->getMemcache()->set($key, $tmp_object, $compression, $expire_time) or die ("Failed to save data at the server");
	}

	public function get($key) {
		return $this->getMemcache()->get($key);
	}

	public function delete($key)
	{
		return $this->getMemcache()->delete($key);
	}

	public function getMemcache(){
		return $this->memcache;
	}

	public function setMemcache($memcache){
		return $this->memcache = $memcache;
	}
}

?>