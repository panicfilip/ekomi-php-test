<?php

interface StrategyInterface
{

	public function set($key, $tmp_object);
	public function get($key);
	public function delete($key);

}
?>