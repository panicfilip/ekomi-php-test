<?php
require_once('MemcacheStrategy.php');

class CacheController
{
	private $strategy = null;
	private $cache_obj = null;

	function __construct($cache_obj) {
		$this->cache_obj = $cache_obj;
		switch ($cache_obj->type) {
			case 'memcache':
				$this->strategy = new MemcacheStrategy();
				break;
			
			default:
				throw new Exception('Cache type is not forwarded.');
				break;
		}
	}

	public function set() {
		return $this->strategy->set($this->cache_obj->key,$this->cache_obj->data);
	}

	public function get() {
		return $this->strategy->get($this->cache_obj->key);
	}

	public function delete() {
		return $this->strategy->delete($this->cache_obj->key);
	}

}

?>