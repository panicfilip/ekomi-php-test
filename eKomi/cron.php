<?php
require_once ('core/MySQLiDB.php');
require_once ('core/TwitterWrapper.php');
require_once ('core/CacheController.php');

$db = new MySQLiDB ('localhost', 'root', '', 'ekomidb');

$db->orderBy("tweet_id","Desc");
$last_inserted_tweet = $db->getOne ("tweet");

if($last_inserted_tweet){
	$since_id = $last_inserted_tweet['tweet_id'];	
}else{
	$since_id = 1;
}

$settings = array(
    'oauth_access_token' => "73194948-8yioszjKOmGYqxeTOMlih2pqHutCDKHCSIh5kjQtx",
    'oauth_access_token_secret' => "MMxrJ9rRzS66D1Y0xYraD3E1RLefgekD5biEY60xz6jeY",
    'consumer_key' => "xQW6AjkYE8l44NKtSEiEaZfJV",
    'consumer_secret' => "vISm9OY5SaHgNRyNugj8O6VSVDLaCswdkBy1B5b56HqB2a2oo0"
);

$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
$getfield = '?screen_name=eKomi&include_rts=1&count=200&since_id=' . $since_id;
$requestMethod = 'GET';

$twitter = new TwitterWrapper($settings);

$twitter_data =  $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
    ->performRequest();

$twitter_data = json_decode($twitter_data);

//delete cache
$cacheObject = (object) array('type' => 'memcache', 'key' => 'eKomiTweets');
$memcacheStrategy = new CacheController($cacheObject);
$memcacheStrategy->delete();

if(sizeof($twitter_data)>0){

	foreach ($twitter_data as $key => $tweet_data) {

		if(property_exists($tweet_data, 'retweeted_status')){
			$retweeted = 1;
			$tweet_user_name = $tweet_data->retweeted_status->user->name;
			$tweet_user_screen_name = $tweet_data->retweeted_status->user->screen_name;
			$favorite_count = $tweet_data->retweeted_status->favorite_count;
			$profile_image_url = $tweet_data->retweeted_status->user->profile_image_url;
		}else{
			$retweeted = 0;
			$tweet_user_name = $tweet_data->user->name;
			$tweet_user_screen_name = $tweet_data->user->screen_name;
			$favorite_count = $tweet_data->favorite_count;
			$profile_image_url = $tweet_data->user->profile_image_url;
		}
		
		$data = Array ("tweet_id" => $tweet_data->id_str,
	               "tweet_text" => $tweet_data->text,
	               "tweet_date_created" => $tweet_data->created_at,
	               "tweet_user_name" => $tweet_user_name,
	               "tweet_user_screen_name" => $tweet_user_screen_name,
	               "retweeted" => $retweeted,
	               "retweet_count" => $tweet_data->retweet_count,
	               "favorite_count" => $favorite_count,
	               "tweet_user_profile_image_url" => $profile_image_url
		);
		$id = $db->insert ('tweet', $data);
		if($id){
			echo 'tweet was inserted. id=' . $id . '<br>';	
		}else{
			echo 'tweet was not inserted. tweet_id = ' . $tweet_data->id_str . '<br>';
		}
	}
}

$tweets = $db->get('tweet'); //contains an Array of all tweets 

//save tweets to cache
$cacheObject = (object) array('type' => 'memcache', 'key' => 'eKomiTweets', 'data' => $tweets);
$memcacheStrategy = new CacheController($cacheObject);
$memcacheStrategy->set();

?>